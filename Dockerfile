FROM ubuntu:trusty

MAINTAINER Mohd Rozi <blackrosezy@gmail.com>

RUN echo "deb http://ppa.launchpad.net/nginx/stable/ubuntu $(lsb_release -cs) main" > /etc/apt/sources.list.d/nginx-stable-$(lsb_release -cs).list

RUN apt-key adv --keyserver keyserver.ubuntu.com --recv-keys C300EE8C

RUN echo "postfix postfix/mailname        string  app.localhost.localdomain" | debconf-set-selections

RUN echo "postfix postfix/main_mailer_type        select  Internet Site" | debconf-set-selections

RUN apt-get update && DEBIAN_FRONTEND=noninteractive ; apt-get -y install \
    nginx libjansson-dev libpcre3-dev libssl-dev libyaml-dev libz-dev \
    build-essential python-pip python python-dev libmysqlclient-dev \
    mysql-client-5.5 redis-tools dos2unix postfix syslog-ng syslog-ng-core \
    && rm -rf /var/lib/apt/lists/*
	
RUN pip install uwsgi supervisor dumb-init

RUN mkdir -p /var/log/supervisor

# Replace the system() source because inside Docker we can't access /proc/kmsg.
# https://groups.google.com/forum/#!topic/docker-user/446yoB0Vx6w
RUN	sed -i -E 's/^(\s*)system\(\);/\1unix-stream("\/dev\/log");/' /etc/syslog-ng/syslog-ng.conf

# Uncomment 'SYSLOGNG_OPTS="--no-caps"' 
RUN	sed -i 's/^#\(SYSLOGNG_OPTS="--no-caps"\)/\1/g' /etc/default/syslog-ng